
### 功能介绍

#### Check list 支持

```
- [x] 完成代码重构
- [ ] 看书 30 页
- [x] 跑步 5 公里
```

得到结果：

- [x] 完成代码重构
- [ ] 看书 30 页
- [x] 跑步 5 公里

#### Tex 支持

使用 [KaTeX](https://khan.github.io/KaTeX/) 渲染数学公式。你可以在 [Function Support in KaTeX](https://khan.github.io/KaTeX/function-support.html) 找到所有支持的功能。

为了快速找到文章中的数学公式，制定了以下规则：

#### 行内公式：

将 Tex 放置在行内代码块中，为了标识这是一个公式，需要在前后加上 `$` 符号。如：\`$a+b = c$\`，得到 `$a+b = c$`

#### 块状公式：

将块状公式用块状代码块包裹，并设语言为 `tex`，如：

<pre><code>```tex
x = {-b \pm \sqrt{b^2-4ac} \over 2a}.
```</code></pre>

得到：

```tex
x = {-b \pm \sqrt{b^2-4ac} \over 2a}.
```

### 代码高亮

```python
class Student(object):
    def __init__(self, name, score):
        self.name = name
        self.score = score

    def print_score(self):
        print('%s: %s' % (self.name, self.score))
```

### 打印成 PDF

为打印加入了专门的 `CSS`，可以将文档打印为排版精美的 PDF，只需按下 `Ctrl + P`。

