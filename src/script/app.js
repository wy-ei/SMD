require.config({
    map: {
        '*': {
            'css': 'https://cdn.bootcss.com/require-css/0.1.8/css.min.js'
        }
    }
});


function APP(){
    this.setImageDescription();
    this.renderMath();
    this.addEventListener();

}

APP.prototype.renderMath = function(){
    
};

APP.prototype.addEventListener = function(){

};

APP.prototype.setImageDescription = function () {
    // 将图片的 alt 属性内容添加在图片下方
    var imgs = document.querySelectorAll('.m-post img');
    for (var i = 0, len = imgs.length; i < len; i++) {
        var alt = imgs[i].getAttribute('alt') || '';
        if (alt){
            var div = document.createElement('div');
            div.setAttribute('class', 'img-alt-wrap');
            div.innerHTML = '<p>' + alt + '</p>';
            imgs[i].parentElement.appendChild(div);
        }
    }
};

new APP();
