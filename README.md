## Jekyll Theme - SMD （Super Markdown Developer）.

对于博客，我一直追求朴素、简洁，经过多年的修改我的博客变成了这样，现献出 Jekyll 主题。

点此预览：[https://wy-ei.github.io/](https://wy-ei.github.io/)

![](https://ogufah3ag.qnssl.com/18-6-1/9195089.jpg)


### 使用方法

#### 1. 安装

你可以手动下载安装包：[点击这里](https://github.com/wy-ei/wy-ei.github.io/archive/master.zip)

或者在根目录执行，需要安装 curl 和 node：

```
curl https://raw.githubusercontent.com/wy-ei/smd/master/install.js | node
```

#### 2. 配置

在配置文件 `_config.yml` 中，需要配置的只有两处

```yaml
# 站点名称
title: XXX's Notes

# 个性签名
slogan: 好记性不如烂笔头
```

#### 3. 写博客

在 `_posts` 文件夹下开心地写博客。

### 建议

如有任何建议，请在此项目的 [issues](https://github.com/wy-ei/wmd/issues) 中提出。
