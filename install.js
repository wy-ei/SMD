let fs = require('fs');
let http = require('http');
let https = require('https');
let url = require('url');
let path = require('path');

function mkdir(filepath){
    dir_list = filepath.split('/')
    dir_list.pop()
    path_list = []

    for(let i = 0;i<dir_list.length;i++){
        path_list.push(dir_list.slice(0, i + 1).join('/'))
    }

    for(let i=0;i<path_list.length;i++){
        full_path = path.join(__dirname, path_list[i])
        path_exist = fs.existsSync(full_path)
        
        if(!path_exist){
            fs.mkdirSync(full_path)
        }
    }
}

function download(source_url, dest, options, callback){
    if(typeof options === 'function'){
        callback = options;
        options = {};
    }
    if(typeof callback !== 'function'){
        callback = function(){};
    }
    let url_object = url.parse(source_url);
    let protocol = url_object.protocol;

    let filename = path.parse(url_object.pathname).base;
    let filepath = path.join(dest, filename);
    mkdir(filepath)

    let file = fs.createWriteStream(filepath);
    
    let req = (protocol === 'https:' ? https : http).get(source_url, (res) => {
        if(res.statusCode >= 400){
            fs.unlink(filepath);
            return callback(new Error('Response status was ' + res.statusCode));
        }

        res.pipe(file, callback);
    });

    // timeout -> 10s
    let timeout = options.timeout || 60000;
    req.setTimeout(timeout, ()=>{
        req.abort();
    });
    req.on('error', (error) => {
        callback(error);
    });
}


files = [
"./404.md",
"./about.md",
"./categories.html",
"./index.html",
"./sitemap.xml",
"./src/script/app.js",
"./src/stylesheet/scss/common.scss",
"./src/stylesheet/scss/highlight.scss",
"./src/stylesheet/scss/m-article.scss",
"./src/stylesheet/scss/m-footer.scss",
"./src/stylesheet/scss/m-header.scss",
"./src/stylesheet/scss/m-home.scss",
"./src/stylesheet/scss/m-icon.scss",
"./src/stylesheet/scss/m-list.scss",
"./src/stylesheet/scss/m-post.scss",
"./src/stylesheet/scss/m-site.scss",
"./src/stylesheet/scss/print.scss",
"./src/stylesheet/scss/typo.scss",
"./src/stylesheet/scss/var.scss",
"./src/stylesheet/style.scss",
"./tags.html",
"./_config.yml",
"./_layouts/default.html",
"./_layouts/page.html",
"./_layouts/post.html",
"./_data/link.yml"
];


let base_url = 'https://gitlab.com/wy-ei/SMD/raw/master';

files.forEach((file, i) => {
    setTimeout(()=>{
        let dir_list = file.split('/')
        dir_list.pop()
        download(base_url + file.slice(1), dir_list.join('/'), {
            timeout: 60000
        }, (error) => {
            if (!error){
                console.log("download sucess!  -- " + file)
            }else{
                console.log(error.message)
                console.log("下载出错了，请从 https://gitlab.com/wy-ei/SMD/tree/master 手动下载");
            }
        });
            
    }, i * 100);
});